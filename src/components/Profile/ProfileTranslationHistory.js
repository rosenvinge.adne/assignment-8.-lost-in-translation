import ProfileSingleTranslation from "./ProfileSingleTranslation";
import React from "react";
import {translationHistory} from "../../util/translationHistory";


const ProfileTranslationHistory = () => {

    const allTerms = translationHistory.getHistory()

    return (
        <div className="items-container">
            <ul>
                {allTerms.map((element, index) => (
                    <li>
                        <ProfileSingleTranslation key={index} term={element}/>
                    </li>
                ))}
            </ul>
        </div>
    )
}

export default ProfileTranslationHistory