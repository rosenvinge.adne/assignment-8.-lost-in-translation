import {translationHistory} from "../../util/translationHistory";
import {useState} from "react";
import ProfileTranslationHistory from "./ProfileTranslationHistory";
import {useHistory} from "react-router-dom";
import './Profile.css'


function Profile() {

    const history = useHistory()

    const [userName] = useState(localStorage.getItem('username'))

    const [tranHistory, setHistory] = useState({
        tranHistory: translationHistory.getHistory()
    } || [])

    const clearHistory = () => {
        translationHistory.clearHistory()
        setHistory([])
    }

    const logOut = () => {
        clearHistory()
        localStorage.clear()
        history.push('/')

    }


    return (
        <div className="profile-container">


            <h3>Your Name: {userName}</h3>


            <h3>History</h3>
            <p>The 10 most resent translations sorted by newest</p>
            <ProfileTranslationHistory/>
            <button className="profile-button" onClick={clearHistory}>Clear</button>
            <button className="profile-button" onClick={logOut}>Log out</button>


        </div>
    )

}

export default Profile