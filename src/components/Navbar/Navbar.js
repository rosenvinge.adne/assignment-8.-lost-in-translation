/*
import { Link, NavLink } from "react-router-dom";
import NavbarMenuToggle from "./NavbarMenuToggle";
import { useState } from "react";
*/

import {Link} from "react-router-dom";
import Logo from '../../assets/Logo.png'
import ROUTER_PATH from "../../variables/RouterPath";
import './Navbar.css'

function Navbar() {

    return (
        <header className="navbar-container">

            <p className="header-font app-header-text">

                <img src={Logo}
                     alt="Movie catalogue for testing" width=""
                     height="24"/>&nbsp;

                Lost in Translation

            </p>

            <nav className="dropdown-content" role="navigation" aria-label="main navigation">


                <Link className="navbar-item" to={ROUTER_PATH.TRANSLATOR}>
                    <button className="navbar-link-button">Translator</button>
                </Link>

                <Link className="navbar-item" to={ROUTER_PATH.PROFILE}>
                    <button className="navbar-link-button">My Profile</button>
                </Link>



            </nav>
        </header>

    )
}


export default Navbar