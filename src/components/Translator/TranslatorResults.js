import './TranslatorResults.css'
import TranslatorSingleImage from "./TranslatorSingleImage";
import React from "react";


function TranslatorResults( {letters} ) {

    let listOfLetters = letters.split('')

    return (
        <div className="translate-results-container">

            <div className="translate-results-content">

                {listOfLetters.map((value, index) => (
                    <div className="single-image">
                        <TranslatorSingleImage key={index+value} letter={value} />
                    </div>
                ))}

            </div>

        </div>
    )

}

export default TranslatorResults