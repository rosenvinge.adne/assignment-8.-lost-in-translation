

function characterIsValidLetter(char) {
    return (/[a-zA-Z]/).test(char)
}

const TranslatorSingleImage = ({letter}) => {

    if (characterIsValidLetter(letter)) {
        // I wanted to separate the root in a constant, but this didn't work..
        let imageUrl = require("../../assets/sign/"+letter.toLowerCase()+".png")
        return (
            <img
                width={50}
                id='image'
                src={imageUrl.default}
                alt={letter}
            />
        )

    } else {
        return (<div className={'no-image'}></div>) // this class is the same size as one of the images, it will look like a space
    }


}

export default TranslatorSingleImage