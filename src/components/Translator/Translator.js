import TranslatorResults from "./TranslatorResults";
import './Translator.css'
import {useState} from "react";
import {translationHistory} from "../../util/translationHistory";

function Translator() {

    const [ state, setState ] = useState({
        localPhrase: '',
        startSearch: false
    })

    const onInputChange = e => {
        setState({
            ...state,
            phrase: e.target.value
        })
    }

    const translateButtonPressed = () => {
        translationHistory.addTranslation(state.phrase)
        setState({
            ...state,
            startSearch: true
        })


    }

    return (
        <div className="translator-container">

            <div className="translator-form-container">

                <input
                    className="translator-input-user-name"
                    id="phrase"
                    type="text"
                    onChange={onInputChange}
                    placeholder="   Enter a word or phrase..."
                    onFocus={(e) => e.target.placeholder = ""}
                    onBlur={(e) => e.target.placeholder = "   Enter a word or phrase..."}
                />
                <button className="translate-button" onClick={translateButtonPressed}>Translate</button>

            </div>

            {state.startSearch ? <TranslatorResults letters={translationHistory.getHistory()[0]} /> : <></> }

        </div>
    )
}


export default Translator