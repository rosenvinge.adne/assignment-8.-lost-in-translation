

function NotFound() {
    return(
        <div>
            <h3 className="header-font ">Lost on the path...</h3>
        </div>
    )

}

export default NotFound