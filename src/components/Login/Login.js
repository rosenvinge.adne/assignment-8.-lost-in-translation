import './Login.css'
import {useState} from "react";
import { useHistory } from "react-router-dom";
import ROUTER_PATH from "../../variables/RouterPath";
import IMAGE_LOGO_HELLO from '../../assets/Logo-Hello.png'


function Login ( ){

    let history = useHistory()

    const [ state, setState ] = useState({
        username: ''
    })

    const onInputChange = e => {
        setState({
            ...state,
            [e.target.id]: e.target.value
        })
    }

    const registerSubmit = () => {
        if (state.username > ''){
            localStorage.setItem('username', state.username)
            history.push(ROUTER_PATH.TRANSLATOR)
        }

    }

    return(
      <div className="login-page">

          <div className="login-header">
              <img width={200} height={200} src={IMAGE_LOGO_HELLO} alt="Welcome logo" />
              <div className="login-top-text">
                  <p className="header-font login-header-text">Lost in Translation</p>
                  <p className="get-started-text">Get started</p>
              </div>
          </div>

          <form className="login-form">
              <div className="login-form-container">
                  <input
                      className="login-input-user-name"
                      id="username"
                      type="text"
                      onChange={onInputChange}
                      placeholder=" 👋 | What's your name?"
                      onFocus={(e) => e.target.placeholder = ""}
                      onBlur={(e) => e.target.placeholder = " 👋 | What's your name?"}
                  />
                  <button className="login-button-submit-name" onClick={registerSubmit}>GO ✔</button>
              </div>
          </form>

      </div>
    );
}

export default Login