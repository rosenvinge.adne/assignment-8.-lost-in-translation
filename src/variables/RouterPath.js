
const ROUTER_PATH = {
    LOGIN: '/login',
    TRANSLATOR: '/translator',
    PROFILE: '/profile',
    ERROR: '/error',
    TEST: '/test'
}

export default ROUTER_PATH