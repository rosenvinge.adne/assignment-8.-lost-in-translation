
export const translationHistory =  {
    history: [],
    KEY: 'search-history',

    // Updates the local variable 'history'
    updateHistory: function () {

        const stored = localStorage.getItem(this.KEY)
        if (stored) {
            const decoded = atob(stored)
            this.history = JSON.parse(decoded)
        }
    },

    // updates localStorage
    updateLocalStorage: function (){
        const encoded = btoa(JSON.stringify(this.history))
        localStorage.setItem(this.KEY, encoded)
    },


    addTranslation: function (term) {
        this.history.unshift(term)
        if (this.history.length > 10) this.history.pop()
        this.updateLocalStorage()
    },

    getHistory: function(){
        this.updateHistory()
        return this.history

    },
    clearHistory: function (){
        this.history = []
        this.updateLocalStorage()
    }


}