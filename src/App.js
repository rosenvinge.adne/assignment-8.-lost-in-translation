import './App.css';
import {BrowserRouter, Switch, Redirect} from "react-router-dom";

import Login from "./components/Login/Login";
import Translator from "./components/Translator/Translator";
import Profile from "./components/Profile/Profile";
import NotFound from "./components/NotFound/NotFound";

import {PublicRoute} from "./hoc/PublicRoute";
import {PrivateRoute} from "./hoc/PrivateRoute";

import ROUTER_PATH from "./variables/RouterPath"
import Navbar from "./components/Navbar/Navbar";



function App() {
    return (
        <div className="App">
            <BrowserRouter>

                <div className="app-header">
                    <Navbar />
                </div>

                <div className="app-content">
                    <Switch>
                        <PublicRoute path="/" exact>
                            <Redirect to={ROUTER_PATH.LOGIN}/>
                        </PublicRoute>

                        <PublicRoute path={ROUTER_PATH.TEST}>
                            <h1 className="header-font">HELLO</h1>
                            <p className="body-font">This is the body with a font</p>
                        </PublicRoute>

                        <PrivateRoute path={ROUTER_PATH.TRANSLATOR} component={Translator}/>

                        <PrivateRoute path={ROUTER_PATH.PROFILE} component={Profile}/>

                        <PublicRoute path={ROUTER_PATH.LOGIN} component={Login}/>

                        <PublicRoute path={ROUTER_PATH.ERROR} component = {NotFound} />

                        <PublicRoute path="*"> <Redirect to={ROUTER_PATH.ERROR} /> </PublicRoute>

                    </Switch>
                </div>
            </BrowserRouter>
        </div>
    );
}

export default App;
