import { Redirect, Route } from "react-router-dom";
import ROUTER_PATH from "../variables/RouterPath";

export const PrivateRoute = props => {
    if (localStorage.getItem('username')){
        return <Route {...props} />
    } else{
        return <Redirect to={ROUTER_PATH.LOGIN} />
    }

}
