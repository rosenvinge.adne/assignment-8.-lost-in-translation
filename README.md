# Lost In Translation
#### by Ådne Rosenvinge

## About

This is a web application created in React. This app lets you translate english words from text to sigh (illustrated with images).

After running the app you will need to enter your username, this will be stored in your local storage. Then you can translate as much as you would like. On the profile page you can se a record over your 10 previous translations. Here you have the option to delete the history and to log out. Log out will delete your history.

## Program files

```

    /public/ 
    /src/
        /assets/
            /sign/                  -> all sign images
                a.png
                b.png
                ...
            Logo.png                -> Logos
            Logo-Hello.png
        /components/                -> All components
            /Login/           
            /Navbar/
            /NotFound/
            /Profile/
            /Translator/
        /hoc/
            PrivateRoute.js         -> Wrapper for all private Routes
            PublicRoute.js          -> Wrapper for all public Routes
        /style-rescources/
            colours.css             -> The given colour sceme
        /util/
            translationHistory.js   -> controlles all actions related to localHistory
        /variables/
            RouterPath.js           -> all routerpaths

        App.js                      -> main wrapper, contains the BrouserRouter and all paths
        app.css
        index.js                    -> main program
        index.css
```

## Design

I tried to follow the templates as far as possible, but endt up with a some creative liberties
